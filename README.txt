At this time, this module does not handle the process of retrieving/obtaining the authorized application token pair needed to upload to Vimeo.


* Download PHP API: http://vimeo.com/api/docs/downloads
* Copy vimeo.php to sites/all/libraries/vimeo/

Note: The process to get a Vimeo token pair that you can use to actually upload videos is a little involved
and has not yet been built in any functional way into this application. I recommend downloading the PHP API and 
reviewing my comments below. You may need to create a quick custom PHP form to help with this process. Once
you get the authorized access token/token secret pair, you don't need to do this process anymore.
 
1. Create an Application on Vimeo: http://vimeo.com/api/applications
  Note: In order to  upload videos to Vimeo through an external application, you must request this functionality. Write a good description of what your 
  application does, and how. I submitted mine on Saturday and got approval by Monday, so it's a reasonably quick turnaround.
   
2. Set the API key/API secret from your Vimeo application credentials
  $api_key = media_vimeo_variable_set('api_key', API_KEY);
  $secret = media_vimeo_variable_set('api_secret', API_SECRET);

3. On a form, get and save a request token
Get a request token
$vimeo = new phpVimeo($api_key, $secret);
$token = $vimeo->getRequestToken();

Store in session, or wherever you like-- these are temporary, so doesn't matter.
$_SESSION['oauth_request_token'] = $token['oauth_token'];
$_SESSION['oauth_request_token_secret'] = $token['oauth_token_secret'];

4. Request authorization
$authorize_link = $vimeo->getAuthorizeUrl($_SESSION['oauth_request_token'], 'write');
If you don't have a Callback URL set up in your Vimeo Application API settings, and/or it is not public-facing yet, you'll just need to manually go to this URL.
After you authorize your application, you'll get a verifier code.. "land-XXXXX"
Store this temporarily-- $_REQUEST['oauth_verifier']

5. Confirm Authorization:
$vimeo->setToken($_SESSION['oauth_request_token'], $_SESSION['oauth_request_token_secret']);
$token = $vimeo->getAccessToken($_REQUEST['oauth_verifier']);
// Store these access tokens somewhere-- once you have these you don't need to re-authenticate, and you can just pass these token/token_secret pair when you connect to VimeoAPI
$token = media_vimeo_variable_set('token', $token['oauth_token']);
$token_secret = media_vimeo_variable_set('token_secret', $token['oauth_token_secret']);