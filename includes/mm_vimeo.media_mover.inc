<?php
// $Id$ mm_vimeo.media_mover.inc, 1 2012/03/31 02:44:31 craigmc Exp $
/**
 * @file
 * Functions to implement Media Mover behavior for Media: Vimeo.
 */

/**
 * Upload a video to Vimeo through Media Mover.
 */
function mm_vimeo_upload_video($file, $configuration) {
  // include vimeo library
  _mm_vimeo_set_include_path();

  $title = check_plain($configuration['media_vimeo_default_title']);
  $description = check_plain($configuration['media_vimeo_default_description']);
  $tags = '';
  $node = NULL;

  if ($file['nid']) {
    $node = node_load($file['nid']);
    if ($node) {
      $title = check_plain($node->title);

      $description_field = $configuration['media_vimeo_description_field'];

      if ($configuration['media_vimeo_description_field'] == 'body') {
        $clean_description = check_plain($node->body);
      }
      else if ($node->{$configuration['media_vimeo_description_field']}) {
        $clean_description = check_plain($node->{$configuration['media_vimeo_description_field']}[0]['value']);
      }
      if ($clean_description) {
        $description = $clean_description;
      }

      $tags = array();
      if ($node->taxonomy) {
        foreach ($node->taxonomy as $term) {
          if ($configuration['media_vimeo_vocabs'][$term->vid]) {
            $ntags = explode(' ', $term->name);
            $tags = array_merge($tags, $ntags);
          }
        }
      }
      $tags = array_filter($tags, '_mm_vimeo_filter_tags');
      $tags = check_plain(implode(', ', $tags));
    }
    else {
      watchdog('media_vimeo', 'File !file belongs to node !nid which does not exist anymore.', array('!file' => $filepath, '!nid' => $file['nid']), WATCHDOG_ERROR);
    }
  }
  if ($tags == '') {
    $tags = check_plain($configuration['media_vimeo_default_tags']);
  }

  $api_key = isset($configuration['media_vimeo_api_key']) ? $configuration['media_vimeo_api_key'] : media_vimeo_variable_get('api_key');
  $secret = isset($configuration['media_vimeo_secret']) ? $configuration['media_vimeo_secret'] : media_vimeo_variable_get('api_secret');
  $token = isset($configuration['media_vimeo_token']) ? $configuration['media_vimeo_token'] : media_vimeo_variable_get('token');
  $token_secret = isset($configuration['media_vimeo_token_secret']) ? $configuration['media_vimeo_token_secret'] : media_vimeo_variable_get('token_secret');

  $filepath = media_mover_api_config_current_file($file);
  $fileinfo = pathinfo($filepath);

  $vimeo = new phpVimeo($api_key, $secret, $token, $token_secret);

  try {
    $video_id = $vimeo->upload($filepath);

    if ($video_id) {
      $url = media_vimeo_video_url($video_id);
      //$vimeo->call('vimeo.videos.setPrivacy', array('privacy' => 'nobody', 'video_id' => $video_id));
      $vimeo->call('vimeo.videos.setTitle', array('title' => $title, 'video_id' => $video_id));
      $vimeo->call('vimeo.videos.setDescription', array('description' => $description, 'video_id' => $video_id));
      watchdog('mm_vimeo', 'Uploaded file !file to Vimeo: !video', array('!file' => $fileinfo['basename'], '!video' => l($url, $url)), WATCHDOG_NOTICE, l($title, $url, array('absolute' => TRUE, 'options' => array('target' => '_blank'))));
      return $url;
    }
    else {
      $message = 'Error: Unable to upload video file: '. $filepath .' from node: '. l($node->title, 'node/'. $file['nid']);
      drupal_set_message($message, 'error');
      watchdog('mm_vimeo', $message, NULL, WATCHDOG_ERROR);
    }
  }
  catch (VimeoAPIException $e) {
    $message = 'Error: Vimeo: Unable to upload: '. $filepath .' from node: '. l($node->title, 'node/'. $file['nid']) .'<br/>';
    $message .= 'Code: '. $e->getCode() .'-'. $e->getMessage();
    drupal_set_message($message, 'error');
    watchdog('mm_vimeo', $message, NULL, WATCHDOG_ERROR);
  }
}

/**
 * Media Mover configuration form element for Media: YouTube.
 *
 * @see media_vimeo_validate_configuration().
 */
function mm_vimeo_config($configuration) {
  $api_key = isset($configuration['media_vimeo_api_key']) ? $configuration['media_vimeo_api_key'] : media_vimeo_variable_get('api_key');
  $secret = isset($configuration['media_vimeo_secret']) ? $configuration['media_vimeo_secret'] : media_vimeo_variable_get('api_secret');
  $token = isset($configuration['media_vimeo_token']) ? $configuration['media_vimeo_token'] : media_vimeo_variable_get('token');
  $token_secret = isset($configuration['media_vimeo_token_secret']) ? $configuration['media_vimeo_token_secret'] : media_vimeo_variable_get('token_secret');

  $form['media_vimeo_conf'] = array(
      '#type' => 'fieldset',
      '#title' => t('Upload to Vimeo configuration'),
      '#element_validate' => array('media_vimeo_validate_configuration', array('media_vimeo_conf')),
  );

  $form['media_vimeo_conf']['media_vimeo_api_key'] = array(
    '#title' => t('Vimeo API Key'),
    '#type' => 'textfield',
    '#default_value' => $api_key,
    '#description' => t('Your Vimeo API Key.'),
  );

  $form['media_vimeo_conf']['media_vimeo_secret'] = array(
    '#title' => t('Vimeo API Shared Secret Key'),
    '#type' => 'textfield',
    '#default_value' => $secret,
    '#description' => t('Your Vimeo API shared secret.'),
  );

  $form['media_vimeo_conf']['media_vimeo_token'] = array(
      '#title' => t('Vimeo API Authorized application token'),
      '#type' => 'textfield',
      '#default_value' => $token,
      '#description' => t('Your Vimeo API Authorized application token'),
  );

  $form['media_vimeo_conf']['media_vimeo_token_secret'] = array(
      '#title' => t('Vimeo API Authorized application token secret'),
      '#type' => 'textfield',
      '#default_value' => $token_secret,
      '#description' => t('Your Vimeo API Authorized application token secret'),
  );

  $form['media_vimeo_conf']['media_vimeo_default_title'] = array(
      '#title' => t('Default title'),
      '#type' => 'textfield',
      '#default_value' => $configuration['media_vimeo_default_title'] ? $configuration['media_vimeo_default_title'] : '',
      '#description' => t('Videos which do not belong to a node will be given this title.'),
      );

      $options = array(
      'body' => t('<Body>'),
      );
      foreach (content_fields() as $field) {
      $options[$field['field_name']] = $field['widget']['label'];
  }
  $form['media_vimeo_conf']['media_vimeo_description_field'] = array(
    '#title' => t('Description field'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $configuration['media_vimeo_description_field'] ? $configuration['media_vimeo_description_field'] : 'body',
    '#description' => t('The video description will be pulled from this field.'),
  );

  $form['media_vimeo_conf']['media_vimeo_default_description'] = array(
    '#title' => t('Default description'),
    '#type' => 'textfield',
    '#default_value' => $configuration['media_vimeo_default_description'] ? $configuration['media_vimeo_default_description'] : 'Default description',
    '#description' => t('Videos which do not have a node, or text in the body or description field above will be given this description.'),
  );

  $form['media_vimeo_conf']['media_vimeo_default_tags'] = array(
    '#title' => t('Default tags'),
    '#type' => 'textfield',
    '#default_value' => $configuration['media_vimeo_default_tags'] ? $configuration['media_vimeo_default_tags'] : '',
    '#description' => t('Videos which do not belong to a node or which do not have tags will be given these tags. Separate them by space.'),
    );

  $vocabs = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabs as $vocab) {
    $options[$vocab->vid] = $vocab->name;
  }

  $form['media_vimeo_conf']['media_vimeo_vocabs'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Take tags from'),
    '#description' => t('Tags will be taken from the selected vocabularies.'),
    '#options' => $options,
    '#default_value' => $configuration['media_vimeo_vocabs'] ? $configuration['media_vimeo_vocabs'] : array(),
  );

  return $form;
}

function _mm_vimeo_filter_tags($val) {
  if ($val == 'si') {
    return FALSE;
  }
  return TRUE;
}
